jQuery(document).on('ready',function(){

//  header-menu
    jQuery(".hover-bg").hide();
    jQuery(".header-nav").hover(function(){
        jQuery(this).addClass('active');
        jQuery(".active >.hover-bg").fadeIn(400);
    }, function(){
        jQuery(this).removeClass('active');
        jQuery(".hover-bg").fadeOut(0);
    });

// fixed nav menu
    var height_header = jQuery("header").outerHeight();
    var h_mrg = 0;
    jQuery(window).on('scroll', function(){
        var top = jQuery(this).scrollTop();
        var elem = jQuery('.navigation');
        if (top+h_mrg < height_header) {
            elem.css('top', (height_header-top));
        } else {
            elem.css('top', h_mrg);
        }
    });

    jQuery(".navbar-nav li").on('click', function () {
        jQuery("#navbarCollapse").removeClass('in');
    });


//feature block
    jQuery(".content-feature").hover(function(){
        jQuery(this).addClass('active');
        jQuery(".active > .feature-hover").fadeIn(400);
    }, function(){
        jQuery(".active > .feature-hover").fadeOut(0);
        jQuery(this).removeClass('active');
    });

//  button top
    var top_show = 800;
    var delay = 1500;
    jQuery(window).on('scroll', function () {
        if (jQuery(this).scrollTop() > top_show) jQuery('#top').fadeIn();
        else jQuery('#top').fadeOut();
    });

    jQuery('#top').on('click', function () {
        jQuery('body, html').animate({
            scrollTop: 0
        }, delay);
    });

//scroll
    jQuery("#navbarCollapse, .header-nav, .arrow-bottom").on("click","a", function (event) {
        event.preventDefault();
        var id  = jQuery(this).attr('href');
        var top = jQuery(id).offset().top;
        jQuery('body,html').animate({scrollTop: top}, 1500);
    });

//portfolio
    jQuery(".hover-portfolio-bg").hide();
    jQuery(".wrapper-img").hover(function(){
        jQuery(this).addClass('active');
        jQuery(".active > .hover-portfolio-bg").fadeIn(400);
    }, function(){
        jQuery(".active > .hover-portfolio-bg").fadeOut(0);
        jQuery(this).removeClass('active');
    });

});

