<?php

function hook_up_js()
{
    wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css' );

    wp_enqueue_style( 'style.less', get_template_directory_uri() . '/less/index.less' );

    wp_enqueue_style( 'jquery.fancybox', get_template_directory_uri() . '/css/fancybox/jquery.fancybox-1.3.4.css' );

    wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap/css/bootstrap.min.css' );

    wp_enqueue_script('header-menu', get_template_directory_uri() . '/js/header-menu.js', array('jquery') );

    wp_enqueue_script( 'fixed-nav-menu', get_template_directory_uri() . '/js/fixed-nav-menu.js', array('jquery') );

    wp_enqueue_script('feature', get_template_directory_uri() . '/js/feature.js', array('jquery') );

    wp_enqueue_script( 'scroll', get_template_directory_uri() . '/js/scroll.js', array('jquery') );

    wp_enqueue_script( 'portfolio', get_template_directory_uri() . '/js/portfolio.js', array('jquery') );

    wp_enqueue_script( 'button-top', get_template_directory_uri() . '/js/button-top.js', array('jquery') );

    wp_enqueue_script( 'bootstrap-min', get_template_directory_uri() . '/css/bootstrap/js/bootstrap.min.js', array('jquery') );

    wp_enqueue_script( 'bootstrap-portfilter-min', get_template_directory_uri() . '/js/bootstrap-portfilter.min.js', array('jquery') );

    wp_enqueue_script( 'jquery.fancybox', get_template_directory_uri() . '/css/fancybox/jquery.mousewheel-3.0.4.pack.js',array('jquery') );

    wp_enqueue_script( 'jquery.ss', get_template_directory_uri() . '/css/fancybox/jquery.fancybox-1.3.4.pack.js', array('jquery') );

}

add_action( 'wp_enqueue_scripts', 'hook_up_js');


//register nav-menu
add_action('after_setup_theme', function(){
    register_nav_menus( array(
        'nav navbar-nav' => 'navbar_nav'
    ));
});


if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'New_Sidebar',
        'id' => "new_sidebar",
    ));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Left_Sidebar',
        'id' => "left_sidebar",
    ));

if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'name' => 'Right_Sidebar',
        'id' => "right_sidebar",
    ));


// register widget
function header_widget()
{
    require_once get_template_directory() . '/Header_Widget.php';
    register_widget( 'Bharagaskara_theme_Header_Widget' );
}
add_action( 'widgets_init', 'header_widget' );



//register thumbnails
add_theme_support('post-thumbnails');



/* Регистрируем post type feature
-----------------------------------------------*/
add_action('init', 'feature');
function feature()
{
    $labels = array(
        'name' => 'Feature',
        'singular_name' => 'Feature',
        'add_new' => 'Add feature',
        'add_new_item' => 'Add new feature',
        'edit_item' => 'edit feature',
        'new_item' => 'New feature',
        'view_item' => 'view feature',
        'search_items' => 'search feature',
        'not_found' =>  'Feature not found',
        'not_found_in_trash' => 'В корзине feature не найдено',
        'parent_item_colon' => '',
        'menu_name' => 'Feature'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title','editor','thumbnail', 'comments'),
        'taxonomies' => array('features')
    );
    register_post_type('feature',$args);
}


//register post type portfolio

add_action('init', 'portfolio');
function portfolio()
{
    $labels = array(
        'name' => 'Portfolio',
        'singular_name' => 'Portfolio',
        'add_new' => 'Add Portfolio',
        'add_new_item' => 'Add new Portfolio',
        'edit_item' => 'edit Portfolio',
        'new_item' => 'New Portfolio',
        'view_item' => 'view Portfolio',
        'search_items' => 'search Portfolio',
        'not_found' =>  'Portfolio not found',
        'not_found_in_trash' => 'В корзине Portfolio не найдено',
        'parent_item_colon' => '',
        'menu_name' => 'Portfolio'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title','editor','thumbnail', 'comments'),
        'taxonomies' => array('portfolios')
    );
    register_post_type('portfolio',$args);
}

//register post type slider reviews

add_action('init', 'reviews');
function reviews()
{
    $labels = array(
        'name' => 'reviews',
        'singular_name' => 'reviews',
        'add_new' => 'Add reviews',
        'add_new_item' => 'Add new reviews',
        'edit_item' => 'edit reviews',
        'new_item' => 'New reviews',
        'view_item' => 'view reviews',
        'search_items' => 'search reviews',
        'not_found' =>  'Reviews not found',
        'not_found_in_trash' => 'В корзине reviews не найдено',
        'parent_item_colon' => '',
        'menu_name' => 'Slider Reviews'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title','editor','thumbnail', 'comments'),
        'taxonomies' => array('reviews_')
    );
    register_post_type('reviews',$args);
}

// register post type slider team
add_action('init', 'team');
function team()
{
    $labels = array(
        'name' => 'team',
        'singular_name' => 'team',
        'add_new' => 'Add team',
        'add_new_item' => 'Add new team',
        'edit_item' => 'edit team',
        'new_item' => 'New team',
        'view_item' => 'view team',
        'search_items' => 'search team',
        'not_found' =>  'Team not found',
        'not_found_in_trash' => 'В корзине team не найдено',
        'parent_item_colon' => '',
        'menu_name' => 'Slider Team'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title','editor','thumbnail', 'comments'),
        'taxonomies' => array('teams')
    );
    register_post_type('team',$args);
}

//register  trim title,content
function trim_title_chars($count, $after)
{
    $title = get_the_title();
    if (mb_strlen($title) > $count) $title = mb_substr($title,0,$count);
    else $after = '';
    echo $title . $after;
}

function trim_content_chars($count, $after)
{
    $content = get_the_content();
    if (mb_strlen($content) > $count) $content = mb_substr($content,0,$count);
    else $after = '';
    echo $content . $after;
}


//ajax open  portfolio
add_action ('wp_ajax_open_portfolio', 'open_portfolio');
add_action ('wp_ajax_nopriv_open_portfolio', 'open_portfolio');

function open_portfolio()
{
    show_portfolio();
    wp_die();
}

function show_portfolio($posts_per_page = false)
{
    $args = array( 'post_type' => 'portfolio');
    if ($posts_per_page) {
        $args['posts_per_page'] = $posts_per_page;
    }
    $portfolio = new WP_Query($args);
    while ($portfolio->have_posts() ) : $portfolio-> the_post(); ?>
        <li data-tag='<?php the_field('data-tag');?>'>
            <div class="col-xs-8 col-sm-offset-2 col-sm-offset-0 col-sm-4 text-center">
                <div class="wrapper-img">
                    <?php $img = wp_get_attachment_image_src( get_post_thumbnail_id());?>
                    <a class="example" href="<?php the_field('img_portfolio');?>"><img src="<?php echo $img[0];?>" class="img-responsive center-block" alt="img"/>
                        <div class="hover-portfolio-bg">
                            <img src="<?php bloginfo('template_url');?>/img/hover-portfolio.png" class="img-responsive center-block"  alt="img"/>
                        </div>
                    </a>
                </div>
                <span class="name-portfolio"><?php trim_title_chars(18,'');?></span>
                <span class="promo-text"><?php trim_content_chars(20,'');?></span>
            </div>
        </li>
    <?php endwhile;
}
