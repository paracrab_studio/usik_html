<?php
class Bharagaskara_theme_Header_Widget extends WP_Widget {
    function __construct() {
        parent::__construct(
// Выбираем ID для своего виджета
            'header_widget',

// Название виджета, показано в консоли
            __('header_widget', 'header_widget_domain'),

// Описание виджета
            array( 'description' => __( 'Виджет для меню', 'header_widget_domain' ), )
        );
    }

// Создаем код для виджета -
// сначала небольшая идентификация
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

// до и после идентификации переменных темой
        echo $args['before_widget'];
        if ( ! empty( $title ) || ! empty( $title ))
// Именно здесь записываем весь код и вывод данных
            ?>

            <div class="header-nav">
                <img src="<?php echo  $instance['img_url'];?>" class="img-icon" alt="img"/>
                <span class="name-menu-header"><?php echo $title = $instance[ 'title' ];?></span>
                <div class="border-menu"></div>
                <a href="#<?php echo $id_block = $instance[ 'id_block' ]; ?>" class="hover-bg"></a>
            </div>

            <?php
        echo $args['after_widget'];
    }

// Закрываем код виджета
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] )  ) {
            $title = $instance[ 'title' ];
        } else {
            $title = __( 'Заголовок виджета', 'img_widget_domain' );

        }
        if ( isset( $instance[ 'img_url' ] )  ) {
            $url = $instance[ 'img_url' ];
        }
        else {
            $url = '';
        }
        if ( isset( $instance[ 'id_block' ] )  ) {
            $id_block = $instance[ 'id_block' ];
        }
        else {
            $id_block = '';
        }
// Для административной консоли
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'img_url' ); ?>"><?php _e( 'Url:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'img_url' ); ?>" name="<?php echo $this->get_field_name( 'img_url' ); ?>"  value = '<?php echo esc_attr( $url ); ?>'/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'id_block' ); ?>"><?php _e( 'id_block' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'id_block' ); ?>" name="<?php echo $this->get_field_name( 'id_block' ); ?>"  value = '<?php echo esc_attr( $id_block ); ?>'/>
        </p>

    <?php
    }

// Обновление виджета
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['img_url'] = ( ! empty( $new_instance['img_url'] ) ) ? strip_tags( $new_instance['img_url'] ) : '';
        $instance['id_block'] = ( ! empty( $new_instance['id_block'] ) ) ? strip_tags( $new_instance['id_block'] ) : '';
        return $instance;
    }
}
