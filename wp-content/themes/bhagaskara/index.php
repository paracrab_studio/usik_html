<!DOCTYPE html>
<html>
<head>
    <title>Bhagaskara</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <?php wp_head(); ?>

</head>
<body>
<header>
    <div id="block-home" class="container-fluid  header-background">
        <div class="container ">
            <div class="rows">
                <div class="logo col-lg-12 text-center">
                    <span>Bhagas<span class="logo-color">kara</span>.</span>
                </div>
                <div class="text-inner-header">
                    <h1 class="text-center">Fusce rutrum pretium lorem semper nulla.</h1>
                    <h2 class="text-center">Vestibulum ornare cursus nisl, fermentum tincidunt libero pretium et. Maecenas interdum ligula non<br/>
                        fermentum fringilla.</h2>
                </div>
            </div>
<!--                widget  header -->
            <div class="row">
                <div class=" hidden-xs col-sm-offset-3   col-md-8 col-md-offset-4  col-lg-8 col-lg-offset-4">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('new_sidebar') ) : ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="hidden-xs col-md-6 col-lg-6">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('left_sidebar') ) : ?>
                    <?php endif; ?>
                </div>
                <div class=" hidden-xs col-md-6 col-lg-6">
                    <div class=" col-md-offset-2  col-lg-offset-2">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('right_sidebar') ) : ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="arrow-bottom hidden-xs col-lg-2 col-lg-offset-5">
                    <a href="#block-feature" class="arrow center-block"></a>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="container-fluid navigation">
    <div class="container">
        <div class="rows">
            <div class="logo-navigation col-xs-5 col-sm-2 col-md-2 col-lg-2">
                <span class="text-center">Bhagas<span class="logo-color">kara</span>.</span>
            </div>
            <div class="menu-navigation col-xs-7 col-sm-10 col-md-10 col-lg-10">
                <nav id="nav" role="navigation" class="navbar navbar-default">
<!--                    mobil menu-->
                    <div class="navbar-header">
                        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbarCollapse" class="test collapse navbar-collapse navbar-right">
<!--                     menu navigation  -->
                        <?php wp_nav_menu( array( 'theme_location' => 'nav navbar-nav', 'container' => false, 'menu_class' => 'nav navbar-nav')); ?>

                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>


<div class="clearfix"></div>

<div id="block-feature" class="container-fluid feature">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

<!--                posts div feature-->
                <?php
                 $feature = new WP_Query( array( 'post_type' => 'feature', 'posts_per_page' => 4 ) );
                while ($feature->have_posts() ) : $feature-> the_post();?>
                     <div class="col-sm-3 col-md-3 col-lg-3">
                        <img src="<?php the_field('img_feature'); ?>" class="img-feature center-block hidden-xs img-responsive" alt="img" />
                        <span class="text-feature-big text-center"><?php the_title();?></span>
                        <span class="text-feature-small text-center"><?php the_content();?></span>
                    </div>
             <?php endwhile;?>
            </div>


        </div>
    </div>
</div>

<div id="about" class="container-fluid about">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <div class="section-title center-block">
                    <img src="<?php bloginfo('template_url');?>/img/44.png" class="angles-right" alt="img"/>
                    <img src="<?php bloginfo('template_url');?>/img/22.png" class="angles-left" alt="img"/>
                    <span class="section-logo">About <span class="logo-color">us</span>.</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7 col-md-7 col-lg-7">
                <img src="<?php bloginfo('template_url');?>/img/screen1.gif" id="screen" class="img-responsive" alt="img"/>
            </div>
            <div class="col-sm-5 col-md-5 col-lg-5 section-text">
                <p>Aliquam metus neque, bibendum sit amet porta at, consequat et
                    enim. In ut turpis non ipsum rhoncus porttitor vel ac nunc.
                    Maecenas interdum dignissim lorem quis auctor. Donec sit amet
                    nulla nisl, aliquam pretium ipsum. Pellentesque sodales ipsum et
                    enim rutrum adipiscing. Quisque tincidunt mattis sapien,
                    vel posuere.</p>
                <a href="#" class="btn btn-lg">Learn more</a>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid section-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo-quote center-block">
                    <img src="<?php bloginfo('template_url');?>/img/img-quoto-1.png" class="icon-quote" alt="img"/>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-8 col-lg-offset-2 text-center">
                <span class="quote-text"> We lorem and ipsum your aliquam</span>
            </div>
        </div>
    </div>
</div>


<div id="team" class="container-fluid team hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-lg-4  col-lg-offset-4">
                <div class="section-title center-block">
                    <img src="<?php bloginfo('template_url');?>/img/44.png" class="angles-right" alt="img"/>
                    <img src="<?php bloginfo('template_url');?>/img/22.png" class="angles-left" alt="img"/>
                    <span class="section-logo">Our <span class="logo-color">team</span>.</span>
                </div>
            </div>
            <div class="col-lg-2 col-lg-offset-5">
                <div class="arrow-slider center-block">
                    <a href="#slider1" class="arrow-left-slider carousel-control" data-slide="prev"></a>
                    <a href="#slider1" class="arrow-right-slider carousel-control" data-slide="next"></a>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div id="slider1" class="carousel slide" data-interval="3000" data-ride="carousel">
                    <div class="carousel-inner">

<!--                        posts slider team -->
                        <div class="active item">
                        <?php $team = new WP_Query( array( 'post_type' => 'team'));
                        $counter = 1;
                        while ($team->have_posts()) : $team-> the_post(); ?>
                            <div class="foto-team col-sm-4 col-md-4 col-lg-4">
                                <img src="<?php the_field('img_team'); ?>" class="img-responsive center-block img-circle img-slider-team" alt="img"/>

                                <span class="name-team"><?php the_title();?></span>
                                <span class="job-post"><?php the_content();?></span>
                                <div class="networks center-block">
                                    <a href="#" class="social-networks"><span class="social-logo">f</span></a>
                                    <a href="#" class="social-networks"><span class="social-logo">g</span></a>
                                </div>
                            </div>
                            <?php
                            if ($counter % 3 == 0 && $counter < $team->post_count) {
                                echo '</div> <div class="item">';
                            }
                        $counter++;
                        endwhile; ?>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="services" class="container-fluid skills">
    <div class="container">
        <div class="row">
            <div class="text-right col-sm-12 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                <div class="section-title center-block">
                    <img src="<?php bloginfo('template_url');?>/img/44.png" class="angles-right" alt="img"/>
                    <img src="<?php bloginfo('template_url');?>/img/22.png" class="angles-left" alt="img"/>
                    <span class="section-logo">Our <span class="logo-color">skill</span>.</span>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row skills-top-row">
            <div class="text-right col-sm-5 col-sm-offset-1 col-md-5  col-lg-5 col-lg-offset-1">
                <span class="category-skill">WEB DESIGN</span>
                <span class="percent">90%</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" style="width: 90%"></div>
                </div>
            </div>
            <div class="text-right col-sm-5    col-md-5  col-lg-5 ">
                <span class="category-skill">GRAPHIC DESIGN</span>
                <span class="percent">70%</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar"  style="width: 70%"></div>
                </div>
            </div>
        </div>
        <div class="row skills-bottom-row">
            <div class=" text-right col-sm-5 col-sm-offset-1 col-md-5 col-lg-5 col-lg-offset-1">
                <span class="category-skill">HTML /CSS</span>
                <span class="percent ">75%</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" style="width: 75%"></div>
                </div>
            </div>
            <div class=" text-right col-sm-5 col-md-5 col-lg-5">
                <span class="category-skill">UI / UX</span>
                <span class="percent">85%</span>
                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar"  style="width: 85%"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid section-img">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="logo-quote center-block">
                    <img src="<?php bloginfo('template_url');?>/img/img-header-menu-1.png" class="icon-quote" alt="img"/>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-8 col-lg-offset-2 text-center">
            <span class="quote-text">Our services will lorem and ipsum your aliquam</span>
        </div>
    </div>
</div>

<div id="blog" class="container-fluid statistic-reviews">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-3">
                <span class="statistic text-center">395</span>
                <span class="statistic-text text-center">Lorem ipsum</span>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <span class="statistic text-center">98%</span>
                <span class="statistic-text text-center">Lorem ipsum</span>
            </div>
            <div class=" col-sm-3 col-md-3 col-lg-3">
                <span class="statistic text-center">69</span>
                <span class="statistic-text text-center">Lorem ipsum</span>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3">
                <span class="statistic text-center">1,642</span>
                <span class="statistic-text text-center">Lorem ipsum</span>
            </div>
        </div>
            <div class="row">
            <div class="col-lg-12 hidden-xs">
                <div id="slider2" class="carousel slide" data-interval="2000" data-ride="carousel">
                    <ol class="carousel-indicators">

<!--                        posts slider reviews-->
                       <?php
                       $reviews = new WP_Query( array( 'post_type' => 'reviews'));
                       $i = 0;
                       while ($reviews->have_posts() ) : $reviews-> the_post();
                           if ($i < $reviews->post_count) { ?>
                               <li data-target="#slider2" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0 ): echo 'active'; endif; ?>"></li>
                          <?php }
                           $i++;
                         endwhile; ?>
                    </ol>
                    <div class="carousel-inner">
                        <?php
                        $posts_per_page = 1;
                        while ($reviews->have_posts() ) : $reviews-> the_post();
                             ?>
                            <div class="<?php if ($posts_per_page == 1 ): echo 'active'; endif; ?> item text-center">
                                <div class="bloc-center">
                                    <img src="<?php the_field('img_reviews')?>" class="img-circle img-responsive img-slider-reviews center-block" alt="img"/>
                                </div>
                                <span class="comment-slider-2"><span class="name-team"><?php the_content();?></span>
                                <span class="name-slider-2"><?php the_title();?></span>
                            </div>
                            <?php  $posts_per_page++;
                         endwhile;?>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


<div id="portfolio" class="container portfolio">
    <div class="row">
        <div class="col-xs-12">
            <div class="section-title center-block">
                <img src="<?php bloginfo('template_url');?>/img/44.png" class="angles-right" alt="img"/>
                <img src="<?php bloginfo('template_url');?>/img/22.png" class="angles-left" alt="img"/>
                <span class="section-logo">Our <span class="logo-color">portfolio</span>.</span>
            </div>
        </div>
    </div>
    <div class="wrapper col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
                <div class="row-button">
                    <button type="button" class="btn btn-default" data-toggle="portfilter" data-target="all">ALL</button>
                    <button type="button" class="btn btn-default" data-toggle="portfilter" data-target="web">WEB</button>
                    <button type="button" class="btn btn-default" data-toggle="portfilter" data-target="apps">APPS</button>
                    <button type="button" class="btn btn-default" data-toggle="portfilter" data-target="icons">ICONS</button>
                </div>
            </div>
        </div>
        <div class="row">
            <ul class="list-portfolio col-lg-12">

<!--                posts portfolio-->
                             <?php show_portfolio(6); ?>

            </ul>
        </div>
        <div class="col-lg-12">
            <div class="portfolio-button center-block">
                <a href="#" class="btn btn-lg open-portfolio">Watch more</a>
            </div>
        </div>
    </div>
</div>


<div id="contact" class="container get-in-touch">
    <div class="row">
        <div class="col-xs-12">
            <div class="section-title center-block">
                <img src="<?php bloginfo('template_url');?>/img/44.png" class="angles-right" alt="img"/>
                <img src="<?php bloginfo('template_url');?>/img/22.png" class="angles-left" alt="img"/>
                <span class="section-logo">Get <span class="logo-color">in touch</span>.</span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Your name">
            </div>
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Your email">
            </div>
            <div class="form-group">
                <textarea class="form-control textarea" rows="13"  placeholder="Your Message"></textarea>
            </div>
            <a href="#" class="btn btn-lg">Send email</a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <span class="text-get-in-touch">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros.
                Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</span>
            <span class="address">1600 Pennsylvania Ave NW, Washington,DC 20500, United States of America.</span>
            <span class="contact">T: (202) 456-1111<br/>
                                  M: (202) 456-1212</span>
            <div class="networks">
                <a href="#" class="social-networks"><span class="social-logo">f</span></a>
                <a href="#" class="social-networks"><span class="social-logo">t</span></a>
                <a href="#" class="social-networks"><span class="social-logo">g</span></a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="text-center">
            <span class="logo-bottom">Bhagas<span class="logo-color">kara</span>.</span>
        </div>
    </div>
</div>
<div id="top">Наверх</div>
<?php wp_footer(); ?>
</body>
</html>

