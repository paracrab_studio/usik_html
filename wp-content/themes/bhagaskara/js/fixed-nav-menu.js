jQuery(document).on('ready',function(){

    var height_header = jQuery("header").outerHeight();
    var h_mrg = 0;
    jQuery(window).on('scroll', function(){
        var top = jQuery(this).scrollTop();
        var elem = jQuery('.navigation');
        if (top+h_mrg < height_header) {
            elem.css('top', (height_header-top));
        } else {
            elem.css('top', h_mrg);
        }
    });

//mobil
    jQuery(".navbar-nav li").on('click', function () {
        jQuery("#navbarCollapse").removeClass('in');
    });

});