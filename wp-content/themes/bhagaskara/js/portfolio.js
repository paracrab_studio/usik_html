jQuery(document).on('ready',function(){

function designPortfolio(){
    jQuery(".hover-portfolio-bg").hide();
    jQuery(".example").hover(function(){
        jQuery(this).addClass('active');
        jQuery(".active > .hover-portfolio-bg").fadeIn(400);
    }, function(){
        jQuery(".active > .hover-portfolio-bg").fadeOut(0);
        jQuery(this).removeClass('active');
    });

    jQuery("a.example").fancybox({
        'opacity'		: true,
        'overlayShow'	: false,
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'none'
    });

}
designPortfolio();
    jQuery('.open-portfolio').on('click', function(event) {
        event.preventDefault();
        jQuery.ajax({

            type : "post",
            url : "/wp-admin/admin-ajax.php",
            data : {
                action: 'open_portfolio'
            },
            success: function(response) {
//                console.log(response);
                jQuery('.list-portfolio').html(response);
                designPortfolio();
            }
        });
    });


});