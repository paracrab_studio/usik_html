jQuery(document).on('ready',function(){

    jQuery(".hover-bg").hide();
    jQuery(".header-nav").hover(function(){
        jQuery(this).addClass('active');
        jQuery(".active >.hover-bg").fadeIn(400);
    }, function(){
        jQuery(this).removeClass('active');
        jQuery(".hover-bg").fadeOut(0);
    });

});