jQuery(document).on('ready',function(){

    var top_show = 800;
    var delay = 1500;
    jQuery(window).on('scroll', function () {
        if (jQuery(this).scrollTop() > top_show) jQuery('#top').fadeIn();
        else jQuery('#top').fadeOut();
    });

    jQuery('#top').on('click', function () {
        jQuery('body, html').animate({
            scrollTop: 0
        }, delay);
    });

});