jQuery(document).on('ready',function(){

    jQuery(".content-feature").hover(function(){
        jQuery(this).addClass('active');
        jQuery(".active > .feature-hover").fadeIn(400);
    }, function(){
        jQuery(".active > .feature-hover").fadeOut(0);
        jQuery(this).removeClass('active');
    });

});