jQuery(document).on('ready',function(){

    jQuery("#navbarCollapse, .header-nav, .arrow-bottom").on("click","a", function (event) {
        event.preventDefault();
        var id  = jQuery(this).attr('href');
        var top = jQuery(id).offset().top;
        jQuery('body,html').animate({scrollTop: top}, 1500);
    });

});